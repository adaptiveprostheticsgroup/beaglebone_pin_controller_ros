#!/usr/bin/env python

import time
import sys
import rospy

from pin_controller.msg import PinValue
from pin_controller.msg import GpioState
from pin_controller.msg import GpioCommand


if __name__ == '__main__':
		try:
			print('Initializing')
			rospy.init_node('TestPin', anonymous=True)

			gpioPub = rospy.Publisher('GpioCommand', GpioCommand, queue_size=10)
			print('Starting')
			v = 1
			for i in range(1,100):
				print(str(i) + '  ' + str(v))
				gpioPub.publish(GpioCommand(pins=[PinValue(id='P8_10', value=v)]))
				v = 1-v
				time.sleep(.4)
			print('Done')
		except:
			sys.exit()
