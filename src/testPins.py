#!/usr/bin/env python

import time
import sys
import rospy

from pin_controller.msg import PinValue
from pin_controller.msg import PwmPin
from pin_controller.msg import GpioState
from pin_controller.msg import AdcState
from pin_controller.msg import GpioCommand
from pin_controller.msg import PwmCommand


if __name__ == '__main__':
		try:
			print('Initializing')
			rospy.init_node('TestPin', anonymous=True)

			pwmPub = rospy.Publisher('PwmCommand', PwmCommand, queue_size=10)
			#gpioPub = rospy.Publisher('GpioCommand', GpioCommand, queue_size=10)
			init = PwmCommand(type=0, pins=[PwmPin(id='P9_14', frequency=1)])
			print(init)
			pwmPub.publish(init)
			time.sleep(5)
			print('Starting')
			for i in range(1,10):
				print('Publishing ' + str(i*10))
				pwmPub.publish(PwmCommand(type=2, pins=[PwmPin(id='P9_14', frequency=i*10)]))
				time.sleep(4)
			print('Done')
		except:
			sys.exit()
