#!/usr/bin/env python
import Adafruit_BBIO.GPIO as gpio
import Adafruit_BBIO.ADC as adc
import Adafruit_BBIO.PWM as pwm

import time
import sys

class corePinController:
	
	def __init__(self):
		self.adcPins = ["P9_40", "P9_39", "P9_38", "P9_37", "P9_36", "P9_35"]
		self.pwmPins = ["P9_14", "P9_16", "P9_21", "P9_22", "P9_29", "P9_31"]
		self.gpioOut = ["P8_10", "P8_11", "P8_12", "P8_13", "P9_14", "P8_15", "P8_16", "P8_17", "P8_18"]
		self.gpioIn = ["P8_27", "P8_28", "P8_29", "P8_30", "P8_31", "P8_32"]
		self.startedPins = []
		adc.setup()

		for pin in self.gpioIn:
			gpio.setup(pin, gpio.IN)
		
		for pin in self.gpioOut:
			gpio.setup(pin, gpio.OUT)


	def cleanUp(self):
		for pin in self.pwmPins:
			pwm.stop(pin)
	
		pwm.cleanup()
		gpio.cleanup()

	def readPins(self):

		values = ','.join(pin + ',' + str(gpio.input(pin)) for pin in self.gpioIn)
	
		first = True
		for pin in self.adcPins:
			# There is a bug in the adc driver that needs to read
			# the values twice in order to get the latest value
#			if not first:
#				values = values + ':'
#			else:
#				first = False

			adc.read(pin)
			values = values + ',' + pin + ',' + str(adc.read(pin))
		print(values)
		sys.stdout.flush()

	def updatePwm(self, commandType, pins):
		# pin = (id, duty, freq, polarity)

		# Set duty cycle (probably the most used to put it first
		if commandType == 1:
			for pin in pins:
				pin = pin.split(',')
				if pin[0] not in self.startedPins:
					pwm.start(pin[0], 0.0, 1, 0)
					self.startedPins.append(pin[0])
				pwm.set_duty_cycle(pin[0], float(pin[1]))
		# Start
		elif commandType == 0:
			for pin in pins:
				pin = pin.split(',')
				if pin[0] not in self.startedPins:
					pwm.start(pin[0], 0.0, 1, 0)
					self.startedPins.append(pin[0])
				pwm.start(pin[0], float(pin[1]), int(pin[2]), int(pin[3]))
	
		# Set Freq
		elif commandType == 2:
			for pin in pins:
				pin = pin.split(',')
				if pin[0] not in self.startedPins:
					pwm.start(pin[0], 0.0, 1, 0)
					self.startedPins.append(pin[0])
				pwm.set_frequency(pin[0], float(pin[2]))

		elif commandType == 3:
			for pin in pins:
				pin = pin.split(',')
				pwm.stop(pin[0])		 

	def write(self, commands):
		commands = commands.split('|')
		
		if len(commands[0]) > 0:
			gpioSection = commands[0].split(':')
			for pair in gpioSection:
				pair = pair.split(',')
				gpio.output(pair[0], int(pair[1]))

		if len(commands[1]) > 0:
			pwmSection = commands[1].split('/')
			self.updatePwm(int(pwmSection[0]), pwmSection[1].split(':'))



if __name__ == '__main__':
	try:
		coreController = corePinController()
		
		while True:
			command = raw_input()
			commandType = command[0]
			if commandType == 'W':
				coreController.write(command[1:])
			elif commandType == 'R':
				coreController.readPins()
			elif commandType == 'X':
				break

		coreController.cleanUp()

	except:
		coreController.cleanUp()
		print(sys.exc_info())










