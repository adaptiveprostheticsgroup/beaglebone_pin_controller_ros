#!/usr/bin/env python

import time
import sys
import rospy
from subprocess import *

from fcntl import fcntl, F_GETFL, F_SETFL
from os import O_NONBLOCK, read

from pin_controller.msg import PinValue
from pin_controller.msg import PwmPin
from pin_controller.msg import GpioState
from pin_controller.msg import AdcState
from pin_controller.msg import GpioCommand
from pin_controller.msg import PwmCommand


class pinController:
	
	def __init__(self):
		self.coreController = Popen(["sudo", "python", "corePinController.py"], stdin=PIPE, stdout=PIPE, shell=False)
		
		flags = fcntl(self.coreController.stdout, F_GETFL)
		fcntl(self.coreController.stdout, F_SETFL, flags | O_NONBLOCK)

		rospy.Subscriber('PwmCommand', PwmCommand, self.updatePwm)
		rospy.Subscriber('GpioCommand', GpioCommand, self.updateGpio)
		self.gpioPub = rospy.Publisher('GpioState', GpioState, queue_size=10)
		self.adcPub = rospy.Publisher('AdcState', AdcState, queue_size=10)

	def cleanUp(self):
		self.coreController.stdin.write('X\n')
		self.gpioPub.unregister()
		self.adcPub.unregister()

	def readSubprocess(self):
		output = read(self.coreController.stdout.fileno(), 1024)
		output = output.split('\n')[0]
		return output.rstrip().split(',')

	def readPins(self):
		self.coreController.stdin.write('R\n')
		output = ''
		gpioRead, adcRead = [], []
		time.sleep(0.0112)

		for i in range(0, 100):
			try:
				output = self.readSubprocess()		
				break
			except:
				time.sleep(0.0001)
				pass

		numPins = len(output)/2
		gpioVals, adcVals = output[:numPins], output[numPins:]
		
		for i in range(0, numPins, 2):
			gpioRead.append(PinValue(id=gpioVals[i], value=int(gpioVals[i + 1])))
		
			adcRead.append(PinValue(id=adcVals[i], value=float(adcVals[i + 1])*1800))

		self.gpioPub.publish(GpioState(pins=gpioRead))
		self.adcPub.publish(AdcState(pins=adcRead))

	def updatePwm(self, command):
		try:
			print('Updated pwm')
			print(command)
	
			s = 'W|' + str(command.type) + '/'
	
			first = True
			for pin in command.pins:
				if not first:
					s = s + ':'
				else:
					first = False
	
				s = s + pin.id + ',' + str(int(pin.duty)) + ',' + str(int(pin.frequency)) + ',' + str(int(pin.polarity))
		except:
			print('Error parsing command')
			print(s)
			return

		print(s)
		try:
			self.coreController.stdin.write(s + '\n')	
			print('Writing to pwm')
		except:
			print('Error writing to pwm')
			return

	def updateGpio(self, command):
		print('Updated gpio')
		print(command)

		s = 'W' + ':'.join(pin.id + ',' + str(int(pin.value)) for pin in command.pins) + '|\n'

		self.coreController.stdin.write(s)
			 

if __name__ == '__main__':
	try:
		controller = pinController()
		
		rospy.init_node('PinController', anonymous=True)

		while not rospy.is_shutdown():
			controller.readPins()

		controller.cleanUp()

	except:
		controller.cleanUp()
		print(sys.exc_info())
