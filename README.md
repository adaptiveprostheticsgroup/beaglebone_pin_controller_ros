# Pin Controller #


This is the ros controller code for the reading/writing the beaglebone pins.

Reading ~50hz for both gpio and adc.

It uses the Adafruit_BBIO python code for the actual hardware changes.

# ROS Messages #

## Commands ##

### PwmCommand/PwmPin ###

- Header header
- int32 type
- PwmPin[] pins

The type in this message refers to the different ways one can update a pwm pin on a beagle bone.

0 - start, calls pwm.start(pinID, duty, frequency, polarity), used to initialize the pin (this will be called if set_duty_cycle or set_frequecy are called on a non initialized pin).

1 - set duty cycle, calls pwm.set_duty_cycle(pinID, duty), (this didnt seem to do anything for the mini servo I tested on but it probably does something)

2 - set frequency, calls pwm.set_frequency(pinID, frequency), This changes the speed and direction of servos.

3 - stop, calls pwm.stop(pinID), used during clean up


### GpioCommand/PinValue ###

- Header header
- PinValue[] pins

The pins contain the id and value to set the gpio pin. On reading this message, the controller calls gpio.output(pinID, value).

## State ##

Both AdcState and GpioState have an array of PinValue, which holds the pinID and value of the pins but in the case of Gpio, the value will be an integer.


#  Scripts #

This script is run as a subprocess as a root user within pinController.py. 
Essentially, pinController asks corePinController to read/write the pins.

The communication between the two is based on passing data packets through the coreController's stdin/stdout.

There are 3 types of packets sent to the corePinController: read (R), write (W), and quit (X).
The read and quit packets are simple:

     R\n
     X\n

When the corePinController reads these, it either reads all of the input pins or stops and cleans up before shutting down.

The write packet has the following form, where <> denote optional characters/variables, ... denotes repeats, and T is the type of update for the pwmPins:

    W<gpioPinId,value:...>|<T/adcPinId,duty,frequency,polarity:...>\n

Note that each pin is separated by ':', and a '|' separates the gpio and adc pins.

In the current system, when a GpioCommand ROS message is received, a write command is sent with only the gpio section filled out and viceversa for an AdcCommand.


For further information contact travnik@ualberta.ca

